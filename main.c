#include "utility.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
  const char *message = "hell<o w&&or>ld";
  const char *mess = message;
  printf("%s\n", mess);

  //string_fun tar inn en input streng og et size av stringen
  char *result = string_fun(mess, (size_t)strlen(message));

  printf("result:\n%s\n", result);
  free(result);
}
