#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *string_fun(const char *arr, size_t len) {
  char find = '&';
  char find2 = '<';
  char find3 = '>';
  char replace[5] = "&amp;";
  char replace2[4] = "&lt;";
  char replace3[4] = "&gt;";
  int strl = (int)len;

  for (int i = 0; i < (int)len; i++) {
    if (arr[i] == find) {
      strl += 4;
    } else if (arr[i] == find2 || arr[i] == find3) {
      strl += 3;
    }
  }

  char *res;
  res = malloc(strl + 1);

  int k = 0;
  for (int i = 0; i < (int)len;) {

    if (arr[i] == find) {

      for (int j = 0; j <= 4; j++, k++) {
        res[k] = replace[j];
        //printf("%c\n", *(res + k));
      }
      i++;
    } else if (arr[i] == find2) {
      for (int j = 0; j <= 3; j++, k++) {
        res[k] = replace2[j];
        //printf("%c\n", *(res + k));
      }
      i++;
    } else if (arr[i] == find3) {
      for (int j = 0; j <= 3; j++, k++) {
        res[k] = replace3[j];
      }
      i++;
    } else {
      res[k] = arr[i];
      k++;
      i++;
    }
  }
  res[k] = '\0';

  return res;
}